import { EDeviceSize } from "../constants";

export class Platform {
    static isWeb(): boolean {
        return true;
    }

    static deviceSize(): EDeviceSize {
        return EDeviceSize.default;
    }
}
