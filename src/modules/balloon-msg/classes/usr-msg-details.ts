import { BalloonMsgLevel, BalloonMsgType } from "../constants";

export class UsrMsgDetails {
    private static nextMsgId = 1;

    msgId: number;
    expired: boolean = false;
    promise: Promise<null> | null = null;

    private rejector: any = null;
    private resolver: any = null;

    constructor(
        public msg: string,
        public level: BalloonMsgLevel,
        public type: BalloonMsgType,
        public confirmText: string | null = null,
        public cancelText: string | null = null,
        public dialogTitle: string | null = null,
        public routePath: string | null = null
    ) {
        this.msgId = UsrMsgDetails.nextMsgId++;

        if (this.confirmText == null) this.confirmText = "Confirm";

        if (this.cancelText == null) this.cancelText = "Cancel";

        this.promise = new Promise<null>((resolver, rejector) => {
            this.resolver = resolver;
            this.rejector = rejector;
        });
    }

    // Level
    isSuccess() {
        return this.level === BalloonMsgLevel.Success;
    }

    isInfo() {
        return this.level === BalloonMsgLevel.Info;
    }

    isWarning() {
        return this.level === BalloonMsgLevel.Warning;
    }

    isError() {
        return this.level === BalloonMsgLevel.Error;
    }

    // Type
    isFleeting() {
        return this.type === BalloonMsgType.Fleeting;
    }

    isSticky() {
        return this.type === BalloonMsgType.Sticky;
    }

    isConfirm() {
        return this.type === BalloonMsgType.Confirm;
    }

    isConfirmCancel() {
        return this.type === BalloonMsgType.ConfirmCancel;
    }

    // Other
    isModal() {
        return this.isConfirm() || this.isConfirmCancel();
    }

    hasRoute() {
        return this.routePath != null;
    }

    resolve() {
        if (this.resolver != null) this.resolver();
    }

    reject() {
        if (this.rejector != null) this.rejector();
    }
}
