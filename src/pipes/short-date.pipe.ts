import { Pipe, PipeTransform } from "@angular/core";
import { DatePipe } from "@angular/common";
import { localTimezoneAsOfDate } from "../utils";

@Pipe({ name: "shortDate" })
export class ShortDatePipe implements PipeTransform {
    constructor(private datePipe: DatePipe) {}

    transform(value: string): string {
        const date = new Date(value);
        return this.datePipe.transform(
            value,
            "EE, dd MMM y",
            localTimezoneAsOfDate(date)
        );
    }
}
