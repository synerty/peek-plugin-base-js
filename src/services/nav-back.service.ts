import { Inject, Injectable } from "@angular/core";
import { NavigationEnd, Router } from "@angular/router";
import { filter } from "rxjs/operators";
import { HeaderService } from "./header.service";

@Injectable({
    providedIn: "root",
})
export class NavBackService {
    private readonly MAX_BACK = 20;
    private backTitles: string[] = [];
    private backUrls: string[] = [];

    constructor(
        @Inject(HeaderService) private headerService,
        @Inject(Router) private router
    ) {
        this.router.events
            .pipe(filter((e) => e instanceof NavigationEnd))
            .subscribe((e: NavigationEnd) => {
                this._recordRouteChange(e);
            });

        // Update the route titles as they come in
        this.headerService.title$.subscribe((title: string) => {
            if (this.backTitles.length == 0) {
                return;
            }

            this.backTitles[this.backTitles.length - 1] = title;
        });
    }

    navBack(count = 1): void {
        if (this.backUrls.length < count) {
            throw new Error(
                `${count} exceeds max nav back ${this.backUrls.length}`
            );
        }

        let url = "";
        for (let i = 0; i <= count; i++) {
            url = this.backUrls.pop();
            this.backTitles.pop();
        }

        this.router.navigateByUrl(url);
    }

    navBackTitles(): string[] {
        return this.backTitles.slice(1);
    }

    navBackLen(): number {
        // The last item on the queue is the current route
        return this.backUrls.length;
    }

    private _recordRouteChange(e: NavigationEnd): void {
        let thisUrl = e.urlAfterRedirects;
        let lastUrl = "";

        if (this.backUrls.length != 0) {
            lastUrl = this.backUrls[this.backUrls.length - 1];
            lastUrl = this._stripUrlParams(lastUrl);
        }

        if (lastUrl == this._stripUrlParams(thisUrl)) {
            this.backUrls[this.backUrls.length - 1] = thisUrl;
            this.backTitles[this.backTitles.length - 1] =
                this.headerService.title;
        } else {
            this.backUrls.push(thisUrl);
            this.backTitles.push(this.headerService.title);
        }

        // This should never happen
        if (this.backTitles.length != this.backUrls.length) {
            throw new Error("backTitles and backUrls length missmatch");
        }

        while (this.backUrls.length > this.MAX_BACK) {
            this.backUrls.shift();
            this.backTitles.shift();
        }
    }

    private _stripUrlParams(url: string): string {
        if (url == null) {
            return "";
        }

        let route: string;

        const params = url.split(";");
        const coordParam = params.find((param: string) =>
            param.includes("coordSetKey")
        );

        route = params[0];

        if (coordParam) {
            if (this.backUrls[this.backUrls.length - 1] === route) {
                this.backUrls.pop();
                this.backTitles.pop();
            }
            route += coordParam;
        }

        return route;
    }
}
