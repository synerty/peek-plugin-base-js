import { Injectable } from "@angular/core";
import { Subject } from "rxjs";
import { UsrMsgDetails } from "../classes";
import { BalloonMsgLevel, BalloonMsgType } from "../constants";
import { IBalloonMsgParams } from "../models";

@Injectable({
    providedIn: "root",
})
export class BalloonMsgService {
    private observable: Subject<UsrMsgDetails>;

    constructor() {
        this.observable = new Subject<UsrMsgDetails>();
    }

    getObservable() {
        return this.observable;
    }

    showError(msg: string, routePath?: string | null): void {
        this.observable.next(
            new UsrMsgDetails(
                msg,
                BalloonMsgLevel.Error,
                BalloonMsgType.Sticky,
                null,
                null,
                null,
                routePath
            )
        );
    }

    showWarning(msg: string, routePath?: string | null): void {
        this.observable.next(
            new UsrMsgDetails(
                msg,
                BalloonMsgLevel.Warning,
                BalloonMsgType.Fleeting,
                null,
                null,
                null,
                routePath
            )
        );
    }

    showInfo(msg: string, routePath?: string | null): void {
        this.observable.next(
            new UsrMsgDetails(
                msg,
                BalloonMsgLevel.Info,
                BalloonMsgType.Fleeting,
                null,
                null,
                null,
                routePath
            )
        );
    }

    showSuccess(msg: string, routePath?: string | null): void {
        this.observable.next(
            new UsrMsgDetails(
                msg,
                BalloonMsgLevel.Success,
                BalloonMsgType.Fleeting,
                null,
                null,
                null,
                routePath
            )
        );
    }

    showMessage(
        msg: string,
        level: BalloonMsgLevel,
        type: BalloonMsgType,
        parameters: IBalloonMsgParams = {}
    ): Promise<null> {
        let { confirmText, cancelText, dialogTitle, routePath } = parameters;
        let msgObj = new UsrMsgDetails(
            msg,
            level,
            type,
            confirmText,
            cancelText,
            dialogTitle,
            routePath
        );
        this.observable.next(msgObj);
        return msgObj.promise;
    }
}
