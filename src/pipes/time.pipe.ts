import { Pipe, PipeTransform } from "@angular/core";
import { DatePipe } from "@angular/common";
import { localTimezoneAsOfDate } from "../utils";

@Pipe({ name: "time" })
export class TimePipe implements PipeTransform {
    constructor(private datePipe: DatePipe) {}

    transform(value: string): string {
        const date = new Date(value);
        return this.datePipe.transform(
            value,
            "HH:mm",
            localTimezoneAsOfDate(date)
        );
    }
}
