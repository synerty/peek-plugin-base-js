export function localTimezoneAsOfDate(date: Date): string {
    // generate a timezone offset in format of [+-]HHMM for DatePipe,
    //  regardless of timzone shifts.

    // e.g. if current timzone is +1200, a date object 14:00+1300 generates
    //       a timezone of +1400 for display as 15:00 through datePipe;
    //      if current timezone is same as the timezone of the date object,
    //       original timezone of the date object is generated
    const recordedDate = date;

    const dateNow = new Date();

    const timezoneOffsetDiff =
        recordedDate.getTimezoneOffset() - dateNow.getTimezoneOffset();
    let timezone = recordedDate.getTimezoneOffset() + timezoneOffsetDiff;

    timezone = -timezone;

    const hour = Math.floor(timezone / 60).toString();
    const minute = Math.floor(timezone % 60).toString();
    const sign = timezone > 0 ? "+" : "-";

    return `${sign}${hour.padStart(2, "0")}${minute.padStart(2, "0")}`;
}
