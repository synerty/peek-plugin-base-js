import { ISound } from "../models";

export class Sound implements ISound {
    private audio: any;

    constructor(soundFilePath: string) {
        this.audio = new Audio(soundFilePath);
    }

    play(): Promise<void> {
        try {
            const optionalPromise = this.audio.play();
            if (optionalPromise != null) return optionalPromise;
        } catch (e) {
            return Promise.reject(e.toString());
        }
        return Promise.resolve();
    }
}
