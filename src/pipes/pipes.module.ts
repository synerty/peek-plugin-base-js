import { NgModule } from "@angular/core";
import { CommonModule, DatePipe } from "@angular/common";
import { FullDatePipe } from "./full-date.pipe";
import { ShortDatePipe } from "./short-date.pipe";
import { TimePipe } from "./time.pipe";

const PIPES = [FullDatePipe, ShortDatePipe, TimePipe];

@NgModule({
    declarations: PIPES,
    imports: [CommonModule],
    providers: [DatePipe],
    exports: PIPES,
})
export class PipesModule {}
