import {
    animate,
    state,
    style,
    transition,
    trigger,
} from "@angular/animations";

export const balloonMsgItemAnimations = [
    trigger("msgPopup", [
        state(
            "void",
            style({
                opacity: 0,
                transform: "scale(2)",
            })
        ),
        state(
            "shown",
            style({
                opacity: 1,
                transform: "scale(1)",
            })
        ),
        state(
            "hidden",
            style({
                opacity: 0,
                transform: "scale(0)",
            })
        ),
        transition("* => *", animate("500ms")),
    ]),
    trigger("msgDialog", [
        state(
            "void",
            style({
                opacity: 0,
            })
        ),
        state(
            "shown",
            style({
                opacity: 1,
            })
        ),
        state(
            "hidden",
            style({
                opacity: 0,
            })
        ),
        transition("* => *", animate("500ms")),
    ]),
];
