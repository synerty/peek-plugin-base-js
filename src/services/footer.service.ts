import { Injectable } from "@angular/core";
import { Subject } from "rxjs";
import { IConfigLink } from "../models";

@Injectable({
    providedIn: "root",
})
export class FooterService {
    statusText = new Subject<string>();
    statusTextSnapshot = "";

    configLinks = new Subject<IConfigLink[]>();
    configLinksSnapshot: IConfigLink[] = [];

    constructor() {}

    setLinks(links: IConfigLink[]) {
        this.configLinksSnapshot = links;
        this.configLinks.next(links);
    }

    setStatusText(title: string) {
        this.statusTextSnapshot = title;
        this.statusText.next(title);
    }
}
