export interface IConfigLink {
    plugin: string;
    route: string;
    text: string;
}

export interface IHeaderLink {
    plugin: string;
    resourcePath: string;
    text: string;
    antIcon: string | null;
    badgeCount: number | null;
}

export interface ISound {
    play(): Promise<void>;
}
