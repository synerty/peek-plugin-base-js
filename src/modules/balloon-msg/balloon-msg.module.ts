import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import {
    BalloonMsgItemComponent,
    BalloonMsgQueueComponent,
} from "./components";

@NgModule({
    declarations: [BalloonMsgQueueComponent, BalloonMsgItemComponent],
    imports: [CommonModule],
    exports: [BalloonMsgQueueComponent, BalloonMsgItemComponent],
})
export class BalloonMsgModule {}
