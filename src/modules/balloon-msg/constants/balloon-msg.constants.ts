export enum BalloonMsgLevel {
    Error = 1,
    Warning = 2,
    Info = 4,
    Success = 8,
}

export enum BalloonMsgType {
    Fleeting = 1,
    Sticky = 2,
    Confirm = 4,
    ConfirmCancel = 8,
}
