export interface IBalloonMsgParams {
    confirmText?: string | null;
    cancelText?: string | null;
    dialogTitle?: string | null;
    routePath?: string | null;
}
