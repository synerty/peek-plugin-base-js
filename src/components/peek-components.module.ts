import { NgModule } from "@angular/core";
import { BrowserModule } from "@angular/platform-browser";
import { RouterModule } from "@angular/router";
import { FormsModule } from "@angular/forms";
import { PeekCheckboxComponent } from "./peek-checkbox";
import { PeekDateInputComponent } from "./peek-date-input";
import { PeekSelectInputComponent } from "./peek-select-input";
import { PeekTextAreaInputComponent } from "./peek-text-area-input";
import { PeekTextInputComponent } from "./peek-text-input";

const COMPONENTS = [
    PeekCheckboxComponent,
    PeekDateInputComponent,
    PeekSelectInputComponent,
    PeekTextAreaInputComponent,
    PeekTextInputComponent,
];

@NgModule({
    declarations: COMPONENTS,
    imports: [RouterModule, FormsModule, BrowserModule],
    exports: COMPONENTS,
})
export class PeekComponentsModule {}
