import { Injectable } from "@angular/core";
import { BehaviorSubject } from "rxjs";
import { IHeaderLink } from "../models";

@Injectable({
    providedIn: "root",
})
export class HeaderService {
    title$ = new BehaviorSubject<string>("no title");
    isEnabled$ = new BehaviorSubject<boolean>(true);
    links$ = new BehaviorSubject<IHeaderLink[]>([]);

    get title() {
        return this.title$.getValue();
    }

    set title(value) {
        this.title$.next(value);
    }

    get isEnabled() {
        return this.isEnabled$.getValue();
    }

    set isEnabled(value) {
        this.isEnabled$.next(value);
    }

    get links() {
        return this.links$.getValue();
    }

    set links(value) {
        this.links$.next(value);
    }

    setLinks(links: IHeaderLink[]): void {
        this.links = links;
    }

    setTitle(title: string): void {
        this.title = title;
    }

    setEnabled(value: boolean) {
        this.isEnabled = value;
    }

    updateBadgeCount(pluginName: string, badgeCount: number | null) {
        const links = [...this.links];

        for (let link of links) {
            if (link.plugin === pluginName) {
                link.badgeCount = badgeCount;
                this.links = links;
                return;
            }
        }
    }
}
